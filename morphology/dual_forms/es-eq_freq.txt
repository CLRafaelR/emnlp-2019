1.
Lemma: -1	MSD: pos=NUM,NumForm=Digit
Form: 3-3-3-1	Freq: 1
Form: 4-2-3-1	Freq: 1

2.
Lemma: aclarar	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: aclarara	Freq: 1
Form: aclarase	Freq: 1

3.
Lemma: admitir	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: admitiera	Freq: 1
Form: admitiese	Freq: 1

4.
Lemma: capturado	MSD: pos=ADJ,Gender=Masc,Number=Sing,VerbForm=Part
Form: capturado	Freq: 2
Form: capturados	Freq: 2

5.
Lemma: decidir	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: decidiera	Freq: 1
Form: decidiese	Freq: 1

6.
Lemma: exhibido	MSD: pos=ADJ,Gender=Masc,Number=Sing,VerbForm=Part
Form: exhibida	Freq: 1
Form: exhibido	Freq: 1

7.
Lemma: ganar	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: ganara	Freq: 1
Form: ganase	Freq: 1

8.
Lemma: mostrar	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: mostrara	Freq: 1
Form: mostrase	Freq: 1

9.
Lemma: naviera	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: naviera	Freq: 1
Form: navieras	Freq: 1

10.
Lemma: percibir	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: percibiera	Freq: 1
Form: percibiese	Freq: 1

11.
Lemma: permitir	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: permitiera	Freq: 2
Form: permitiese	Freq: 2

12.
Lemma: pleno	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: pleno	Freq: 1
Form: plenos	Freq: 1

13.
Lemma: real	MSD: pos=NOUN,NumForm=Digit
Form: real	Freq: 2
Form: reales	Freq: 2

14.
Lemma: sp_km/h	MSD: pos=NUM,NumForm=Digit
Form: km/h	Freq: 1
Form: kms/h	Freq: 1

15.
Lemma: tener_que_ver	MSD: pos=AUX,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: que	Freq: 2
Form: ver	Freq: 2

16.
Lemma: tener_que_ver	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: que	Freq: 1
Form: ver	Freq: 1

17.
Lemma: tener_que_ver	MSD: pos=AUX,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: que	Freq: 1
Form: ver	Freq: 1

18.
Lemma: tratar	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: tratara	Freq: 2
Form: tratase	Freq: 2

19.
Lemma: volver	MSD: pos=VERB,Mood=Sub,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: vuelva	Freq: 1
Form: vuelve	Freq: 1

20.
Lemma: él	MSD: pos=PRON,Number=Plur,Person=3,PronType=Prs
Form: les	Freq: 1
Form: se	Freq: 1
