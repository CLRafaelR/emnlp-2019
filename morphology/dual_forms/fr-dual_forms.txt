1.
Lemma: a	MSD: pos=DET,Number=Sing
Form: a	Freq: 9
Form: an	Freq: 5

2.
Lemma: abandonner	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: abandonné	Freq: 14
Form: abanbonné	Freq: 1

3.
Lemma: accueil	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: accueil	Freq: 55
Form: acceuil	Freq: 3

4.
Lemma: accueillir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: accueille	Freq: 15
Form: accueil	Freq: 1

5.
Lemma: acte	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: actes	Freq: 13
Form: ac.	Freq: 1

6.
Lemma: activité	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: activités	Freq: 40
Form: activitées	Freq: 1

7.
Lemma: amateur	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: amateur	Freq: 1
Form: amateure	Freq: 1

8.
Lemma: animé	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: animé	Freq: 2
Form: anime	Freq: 1

9.
Lemma: antérieur	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: antérieure	Freq: 2
Form: antérieures	Freq: 1

10.
Lemma: août	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: août	Freq: 115
Form: aout	Freq: 3

11.
Lemma: apercevoir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: aperçoit	Freq: 5
Form: apperçoit	Freq: 1

12.
Lemma: apparaître	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: apparaît	Freq: 37
Form: apparait	Freq: 2

13.
Lemma: apprécier	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: apprecie	Freq: 1
Form: apprécie	Freq: 1

14.
Lemma: article	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: article	Freq: 28
Form: art.	Freq: 1

15.
Lemma: assumer	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: assumé	Freq: 2
Form: assumée	Freq: 1

16.
Lemma: atterrir	MSD: pos=VERB,VerbForm=Inf
Form: atterir	Freq: 1
Form: atterrir	Freq: 1

17.
Lemma: avoir	MSD: pos=AUX,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: ont	Freq: 498
Form: on	Freq: 1
Form: unt	Freq: 1

18.
Lemma: avoir	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: a	Freq: 1606
Form: à	Freq: 2
Form: on	Freq: 1

19.
Lemma: avoir	MSD: pos=VERB,Mood=Cnd,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: aurait	Freq: 8
Form: aurais	Freq: 1

20.
Lemma: avoir	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: ont	Freq: 104
Form: non	Freq: 1

21.
Lemma: avoir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: a	Freq: 328
Form: à	Freq: 1

22.
Lemma: aération	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: aération	Freq: 2
Form: aeration	Freq: 1

23.
Lemma: aîné	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: aîné	Freq: 9
Form: ainé	Freq: 2

24.
Lemma: baccalauréat	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: baccalauréat	Freq: 4
Form: baccalaurét	Freq: 1

25.
Lemma: beau	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: beau	Freq: 18
Form: bel	Freq: 5

26.
Lemma: blanc	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: blanche	Freq: 15
Form: blanque	Freq: 1

27.
Lemma: boeuf	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: boeuf	Freq: 2
Form: bœuf	Freq: 1

28.
Lemma: boîte	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: boîte	Freq: 10
Form: boite	Freq: 1

29.
Lemma: bâtisse	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: batisse	Freq: 1
Form: bâtisse	Freq: 1

30.
Lemma: canadien	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: canadienne	Freq: 13
Form: candienne	Freq: 1

31.
Lemma: capturer	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: capturé	Freq: 3
Form: capture	Freq: 1

32.
Lemma: cathédrale	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: cathédrale	Freq: 20
Form: cocathédrale	Freq: 1

33.
Lemma: ce	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Dem
Form: cette	Freq: 773
Form: cet	Freq: 2

34.
Lemma: ce	MSD: pos=DET,Gender=Masc,Number=Sing,PronType=Dem
Form: ce	Freq: 665
Form: cet	Freq: 163

35.
Lemma: ce	MSD: pos=PRON,Number=Sing,Person=3,PronType=Dem
Form: c'	Freq: 487
Form: ce	Freq: 342
Form: -ce	Freq: 24
Form: c	Freq: 1

36.
Lemma: cela	MSD: pos=PRON,Number=Sing,PronType=Dem
Form: cela	Freq: 122
Form: c'	Freq: 1

37.
Lemma: chauffer	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: chauffé	Freq: 2
Form: chauffée	Freq: 1

38.
Lemma: chaîne	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: chaîne	Freq: 32
Form: chaine	Freq: 2

39.
Lemma: cheval	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: chevaux	Freq: 9
Form: ch	Freq: 1

40.
Lemma: château	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: château	Freq: 100
Form: chateau	Freq: 2

41.
Lemma: chèque	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: chèque	Freq: 1
Form: chéque	Freq: 1

42.
Lemma: chœur	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: chœur	Freq: 15
Form: chœeur	Freq: 1

43.
Lemma: cinquième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: cinquième	Freq: 8
Form: 5e	Freq: 1
Form: 5ème	Freq: 1

44.
Lemma: cinquième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: cinquième	Freq: 6
Form: 5e	Freq: 2

45.
Lemma: collège	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: collège	Freq: 22
Form: college	Freq: 2

46.
Lemma: commune	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: commune	Freq: 205
Form: kommune	Freq: 1

47.
Lemma: compléter	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: complété	Freq: 2
Form: complèté	Freq: 1

48.
Lemma: compétition	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: compétitions	Freq: 10
Form: compétitons	Freq: 1

49.
Lemma: compétition	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: compétition	Freq: 31
Form: compétiton	Freq: 1

50.
Lemma: concrétiser	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: concretise	Freq: 1
Form: concrétise	Freq: 1

51.
Lemma: concurrencer	MSD: pos=VERB,VerbForm=Inf
Form: conccurencer	Freq: 1
Form: concurrencer	Freq: 1

52.
Lemma: connaître	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: connaissent	Freq: 8
Form: connaisent	Freq: 1

53.
Lemma: connaître	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: connaît	Freq: 21
Form: connait	Freq: 6

54.
Lemma: connaître	MSD: pos=VERB,VerbForm=Inf
Form: connaître	Freq: 22
Form: connaitre	Freq: 3

55.
Lemma: considérer	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: considéré	Freq: 29
Form: considértés	Freq: 1

56.
Lemma: considérer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: considère	Freq: 16
Form: considére	Freq: 1

57.
Lemma: continuer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: continue	Freq: 17
Form: continu	Freq: 1

58.
Lemma: contrainte	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: contraintes	Freq: 5
Form: contraites	Freq: 1

59.
Lemma: couleur	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: couleur	Freq: 24
Form: coucleur	Freq: 1

60.
Lemma: cousin	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: cousin	Freq: 5
Form: couisn	Freq: 1

61.
Lemma: coût	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: coût	Freq: 20
Form: cout	Freq: 1

62.
Lemma: coûteux	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: couteux	Freq: 2
Form: coûteux	Freq: 2

63.
Lemma: croire	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: croient	Freq: 1
Form: croîent	Freq: 1

64.
Lemma: cruel	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: cruel	Freq: 2
Form: cruels	Freq: 1

65.
Lemma: créer	MSD: pos=VERB,Gender=Masc,Number=Plur,Tense=Past,VerbForm=Part
Form: créés	Freq: 4
Form: crée	Freq: 1

66.
Lemma: cyclisme	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: cyclisme	Freq: 10
Form: ciclisme	Freq: 1

67.
Lemma: célébrité	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: célébrités	Freq: 4
Form: célibrités	Freq: 1

68.
Lemma: côté	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: côtés	Freq: 17
Form: cotés	Freq: 1

69.
Lemma: côté	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: côté	Freq: 80
Form: coté	Freq: 8

70.
Lemma: cœur	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: cœur	Freq: 31
Form: contrecœur	Freq: 1

71.
Lemma: deux	MSD: pos=PRON,Number=Plur
Form: deux	Freq: 9
Form: 2	Freq: 1

72.
Lemma: deuxième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: deuxième	Freq: 44
Form: 2e	Freq: 2
Form: 2ème	Freq: 2

73.
Lemma: deuxième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: deuxième	Freq: 38
Form: 2e	Freq: 2

74.
Lemma: devoir	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: dut	Freq: 7
Form: du	Freq: 1

75.
Lemma: devoir	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: doit	Freq: 95
Form: doivent	Freq: 44

76.
Lemma: devoir	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: dû	Freq: 10
Form: du	Freq: 2

77.
Lemma: disparaître	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: disparaît	Freq: 6
Form: disparait	Freq: 4

78.
Lemma: disparaître	MSD: pos=VERB,VerbForm=Inf
Form: disparaître	Freq: 4
Form: disparaitre	Freq: 1

79.
Lemma: disposer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: dispose	Freq: 22
Form: dispos	Freq: 1

80.
Lemma: dix-septième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: 17e	Freq: 3
Form: 17ème	Freq: 1

81.
Lemma: docteur	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: docteur	Freq: 19
Form: dr.	Freq: 2

82.
Lemma: documentaire	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: documentaire	Freq: 5
Form: docu	Freq: 1

83.
Lemma: douzième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: 12e	Freq: 1
Form: douzième	Freq: 1

84.
Lemma: décoration	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: décoration	Freq: 13
Form: déco	Freq: 1

85.
Lemma: découper	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: découpé	Freq: 2
Form: decoupé	Freq: 1

86.
Lemma: dépôt	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: dépôt	Freq: 3
Form: dépot	Freq: 1

87.
Lemma: désert	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: désert	Freq: 7
Form: desert	Freq: 1

88.
Lemma: désir	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: désir	Freq: 9
Form: désire	Freq: 1

89.
Lemma: détruire	MSD: pos=VERB,Gender=Masc,Number=Plur,Tense=Past,VerbForm=Part
Form: détruits	Freq: 5
Form: détruient	Freq: 1

90.
Lemma: dîner	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: dîner	Freq: 3
Form: diner	Freq: 1

91.
Lemma: employé	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: employés	Freq: 8
Form: empoyés	Freq: 1

92.
Lemma: enregistrement	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: enregistrements	Freq: 8
Form: enrégistrements	Freq: 1

93.
Lemma: ensemble	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: ensemble	Freq: 83
Form: ensmble	Freq: 1

94.
Lemma: entamer	MSD: pos=VERB,VerbForm=Inf
Form: entamer	Freq: 1
Form: entammer	Freq: 1

95.
Lemma: entraînement	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: entraînement	Freq: 12
Form: entrainement	Freq: 1

96.
Lemma: entraîneur	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: entraîneur	Freq: 13
Form: entraineur	Freq: 2

97.
Lemma: environnement	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: environnements	Freq: 3
Form: environements	Freq: 1

98.
Lemma: espérer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Imp,VerbForm=Fin
Form: espérait	Freq: 2
Form: espèrait	Freq: 1

99.
Lemma: essayer	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: essaient	Freq: 2
Form: essayent	Freq: 1

100.
Lemma: essayer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: essaie	Freq: 11
Form: essaye	Freq: 2

101.
Lemma: est	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: est	Freq: 47
Form: e	Freq: 1

102.
Lemma: euro	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: euros	Freq: 56
Form: e	Freq: 1

103.
Lemma: exceptionnel	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: exceptionnel	Freq: 9
Form: exeptionnel	Freq: 1

104.
Lemma: extension	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: extension	Freq: 14
Form: extention	Freq: 1

105.
Lemma: extrême	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: extrême	Freq: 9
Form: extreme	Freq: 1

106.
Lemma: exécuter	MSD: pos=VERB,Gender=Fem,Number=Plur,Tense=Past,VerbForm=Part
Form: exécutées	Freq: 1
Form: exécutés	Freq: 1

107.
Lemma: faire	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: fait	Freq: 138
Form: fais	Freq: 1

108.
Lemma: façon	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: façon	Freq: 45
Form: facon	Freq: 1

109.
Lemma: file	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: file	Freq: 3
Form: fil	Freq: 1

110.
Lemma: fils	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: fils	Freq: 76
Form: fis	Freq: 1

111.
Lemma: frais	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: fraiches	Freq: 2
Form: fraîches	Freq: 1

112.
Lemma: franc	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: franque	Freq: 4
Form: franche	Freq: 1

113.
Lemma: franc	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: francs	Freq: 10
Form: frs	Freq: 1

114.
Lemma: fraîcheur	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: fraîcheur	Freq: 3
Form: fraicheur	Freq: 1

115.
Lemma: go	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: go	Freq: 2
Form: go.	Freq: 1

116.
Lemma: gouverneur	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: gouverneur	Freq: 27
Form: gouver-neur	Freq: 1

117.
Lemma: grand-chef	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: grand-chefs	Freq: 1
Form: grands-chefs	Freq: 1

118.
Lemma: groupe	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: groupe	Freq: 201
Form: goupe	Freq: 1

119.
Lemma: grâce	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: grâce	Freq: 90
Form: grace	Freq: 1

120.
Lemma: guider	MSD: pos=VERB,Gender=Fem,Number=Sing,Tense=Past,VerbForm=Part
Form: guidée	Freq: 1
Form: guidées	Freq: 1

121.
Lemma: harmonie	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: harmonie	Freq: 3
Form: armonie	Freq: 1

122.
Lemma: heure	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: heures	Freq: 42
Form: h	Freq: 1

123.
Lemma: heure	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: heure	Freq: 23
Form: h	Freq: 22

124.
Lemma: héritier	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: héritier	Freq: 7
Form: hériter	Freq: 1

125.
Lemma: hôtel	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: hôtel	Freq: 47
Form: hotel	Freq: 5

126.
Lemma: i	MSD: pos=PRON,PronType=Prs
Form: me	Freq: 4
Form: i	Freq: 1

127.
Lemma: il	MSD: pos=PRON,Gender=Fem,Number=Plur,Person=3,PronType=Prs
Form: elles	Freq: 108
Form: -elles	Freq: 2

128.
Lemma: il	MSD: pos=PRON,Gender=Fem,Number=Sing,Person=3,PronType=Prs
Form: -elle	Freq: 33
Form: elle	Freq: 1

129.
Lemma: il	MSD: pos=PRON,Gender=Masc,Number=Plur,Person=3,PronType=Prs
Form: ils	Freq: 449
Form: -ils	Freq: 19

130.
Lemma: il	MSD: pos=PRON,Gender=Masc,Number=Sing,Person=3,PronType=Prs
Form: il	Freq: 3229
Form: -il	Freq: 103
Form: lui	Freq: 2
Form: t-il	Freq: 2

131.
Lemma: impeccable	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: impeccable	Freq: 2
Form: impeccaple	Freq: 1

132.
Lemma: inclure	MSD: pos=VERB,Tense=Pres,VerbForm=Part
Form: incluant	Freq: 5
Form: incluent	Freq: 1

133.
Lemma: ingéniérie	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: ingénierie	Freq: 1
Form: ingéniérie	Freq: 1

134.
Lemma: inondation	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: innondation	Freq: 1
Form: inondation	Freq: 1

135.
Lemma: inscrire	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: inscrit	Freq: 13
Form: inscrivent	Freq: 1

136.
Lemma: interpeller	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: interpellé	Freq: 1
Form: interpelé	Freq: 1

137.
Lemma: intéresser	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: interessa	Freq: 1
Form: intéressa	Freq: 1

138.
Lemma: issue	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: issue	Freq: 17
Form: issu	Freq: 1

139.
Lemma: je	MSD: pos=PRON,Number=Sing,Person=1,PronType=Prs
Form: j'	Freq: 203
Form: -je	Freq: 7
Form: je	Freq: 3
Form: j	Freq: 1

140.
Lemma: jour	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: jours	Freq: 94
Form: jrs	Freq: 1

141.
Lemma: kilomètre	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: kilomètres	Freq: 44
Form: kms	Freq: 2

142.
Lemma: kilomètre/heure	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: km/h	Freq: 2
Form: kilomètres-heure	Freq: 1

143.
Lemma: le	MSD: pos=DET,Definite=Def,Gender=Fem,Number=Plur,PronType=Art
Form: les	Freq: 3015
Form: l	Freq: 1

144.
Lemma: le	MSD: pos=DET,Definite=Def,Gender=Fem,Number=Sing,PronType=Art
Form: la	Freq: 9706
Form: l'	Freq: 2821
Form: l	Freq: 13
Form: les	Freq: 1
Form: là	Freq: 1

145.
Lemma: le	MSD: pos=DET,Definite=Def,Gender=Masc,Number=Sing,PronType=Art
Form: le	Freq: 13699
Form: l'	Freq: 2152
Form: l	Freq: 13

146.
Lemma: le	MSD: pos=DET,Definite=Def,Number=Sing,PronType=Art
Form: l'	Freq: 1211
Form: l	Freq: 2

147.
Lemma: le	MSD: pos=PRON,Gender=Fem,Number=Sing,Person=3,PronType=Prs
Form: la	Freq: 93
Form: l'	Freq: 1

148.
Lemma: le	MSD: pos=PRON,Gender=Masc,Number=Sing,Person=3,PronType=Prs
Form: le	Freq: 257
Form: l'	Freq: 3
Form: -le	Freq: 1

149.
Lemma: le	MSD: pos=PRON,Number=Plur,Person=1,PronType=Prs
Form: nous	Freq: 35
Form: -nous	Freq: 1

150.
Lemma: le	MSD: pos=PRON,Number=Plur,Person=2,PronType=Prs
Form: vous	Freq: 30
Form: -vous	Freq: 3

151.
Lemma: le	MSD: pos=PRON,Number=Sing,Person=3,PronType=Prs
Form: l'	Freq: 234
Form: le	Freq: 1

152.
Lemma: lequel	MSD: pos=PRON,Gender=Fem,Number=Sing
Form: laquelle	Freq: 10
Form: lesquelles	Freq: 1

153.
Lemma: lignée	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: lignée	Freq: 6
Form: ligneé	Freq: 1

154.
Lemma: litre	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: litres	Freq: 5
Form: l	Freq: 2

155.
Lemma: lui	MSD: pos=PRON,Number=Plur,Person=2,PronType=Prs
Form: vous	Freq: 56
Form: -vous	Freq: 1

156.
Lemma: léger	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: léger	Freq: 3
Form: légèr	Freq: 1

157.
Lemma: lépidoptère	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: lépidoptères	Freq: 5
Form: lepidoptères	Freq: 1

158.
Lemma: m	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: m	Freq: 65
Form: team	Freq: 5
Form: prism	Freq: 2
Form: rhythm	Freq: 2
Form: 120km	Freq: 1
Form: bunodontum	Freq: 1
Form: limbum	Freq: 1
Form: nm	Freq: 1
Form: onm	Freq: 1
Form: pistum	Freq: 1
Form: pm	Freq: 1
Form: pom-pom	Freq: 1
Form: tétraméthylammonium	Freq: 1
Form: verbum	Freq: 1

159.
Lemma: maintien	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: maintien	Freq: 8
Form: maintient	Freq: 1

160.
Lemma: majorité	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: majorité	Freq: 49
Form: majortié	Freq: 1

161.
Lemma: marchand	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: marchand	Freq: 5
Form: machand	Freq: 1

162.
Lemma: marocain	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: marocaine	Freq: 18
Form: maocaine	Freq: 1

163.
Lemma: marron	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: marrons	Freq: 2
Form: marron	Freq: 1

164.
Lemma: match	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: matchs	Freq: 40
Form: matches	Freq: 11

165.
Lemma: maître	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: maître	Freq: 33
Form: maitre	Freq: 1

166.
Lemma: me	MSD: pos=PRON,Number=Sing,Person=1,PronType=Prs
Form: m'	Freq: 48
Form: me	Freq: 3

167.
Lemma: mettre	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: met	Freq: 46
Form: mets	Freq: 1

168.
Lemma: mile	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: mi	Freq: 1
Form: miles	Freq: 1

169.
Lemma: milliard	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: milliards	Freq: 32
Form: milliard	Freq: 1

170.
Lemma: milliard	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: milliard	Freq: 2
Form: milliards	Freq: 1

171.
Lemma: million	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: millions	Freq: 69
Form: million	Freq: 1

172.
Lemma: minstrel	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: minstrel	Freq: 1
Form: minstrelsy	Freq: 1

173.
Lemma: minute	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: minutes	Freq: 36
Form: min	Freq: 1
Form: mins	Freq: 1

174.
Lemma: minute	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: minute	Freq: 11
Form: min	Freq: 1

175.
Lemma: minéral	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: minéraux	Freq: 3
Form: minéral	Freq: 2

176.
Lemma: mondial	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: mondial	Freq: 1
Form: mondiaux	Freq: 1

177.
Lemma: monsieur	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: madame	Freq: 5
Form: mme	Freq: 4
Form: mademoiselle	Freq: 1

178.
Lemma: mourir	MSD: pos=VERB,Gender=Masc,Number=Plur,Tense=Past,VerbForm=Part
Form: mort	Freq: 4
Form: morts	Freq: 3

179.
Lemma: mémoire	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: mémoire	Freq: 3
Form: mémoires	Freq: 1

180.
Lemma: mériter	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: méritent	Freq: 2
Form: merite	Freq: 1

181.
Lemma: métal	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: métal	Freq: 8
Form: metal	Freq: 1

182.
Lemma: même	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: même	Freq: 102
Form: meme	Freq: 2

183.
Lemma: naître	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: né	Freq: 254
Form: ne	Freq: 1

184.
Lemma: ne	MSD: pos=ADV,Polarity=Neg
Form: ne	Freq: 722
Form: n'	Freq: 707
Form: de	Freq: 1

185.
Lemma: neuvième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: neuvième	Freq: 2
Form: 9e	Freq: 1
Form: 9ème	Freq: 1

186.
Lemma: noeud	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: nœuds	Freq: 3
Form: noeuds	Freq: 1

187.
Lemma: nord	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: nord	Freq: 95
Form: n	Freq: 1

188.
Lemma: normand	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: normande	Freq: 4
Form: normand	Freq: 2

189.
Lemma: nourriture	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: nourriture	Freq: 19
Form: nouriture	Freq: 1

190.
Lemma: nous	MSD: pos=PRON,Number=Plur,Person=1,PronType=Prs
Form: nous	Freq: 235
Form: -nous	Freq: 2

191.
Lemma: nouveau	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: nouveau	Freq: 125
Form: nouvel	Freq: 19

192.
Lemma: novembre	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: novembre	Freq: 99
Form: nov	Freq: 1

193.
Lemma: numéro	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: numéro	Freq: 18
Form: n°	Freq: 1

194.
Lemma: négatif	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: négatif	Freq: 7
Form: negatif	Freq: 1

195.
Lemma: on	MSD: pos=PRON,Gender=Masc,Number=Sing,Person=3
Form: on	Freq: 537
Form: -on	Freq: 21

196.
Lemma: onzième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: 11	Freq: 1
Form: onzième	Freq: 1

197.
Lemma: où	MSD: pos=PRON,PronType=Rel
Form: où	Freq: 315
Form: ou	Freq: 6
Form: oà	Freq: 1

198.
Lemma: parcourir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: parcourt	Freq: 5
Form: parcours	Freq: 2

199.
Lemma: parenthèse	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: paranthèses	Freq: 1
Form: parenthèses	Freq: 1

200.
Lemma: payer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: paie	Freq: 4
Form: paye	Freq: 3

201.
Lemma: paysager	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: paysager	Freq: 1
Form: paysagé	Freq: 1

202.
Lemma: permettre	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Fut,VerbForm=Fin
Form: permettront	Freq: 6
Form: permettrons	Freq: 1

203.
Lemma: personnel	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: personnel	Freq: 23
Form: personel	Freq: 1

204.
Lemma: plaire	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: plaît	Freq: 2
Form: plait	Freq: 1

205.
Lemma: pointer	MSD: pos=VERB,Tense=Pres,VerbForm=Part
Form: pointant	Freq: 1
Form: pointants	Freq: 1

206.
Lemma: pouvoir	MSD: pos=AUX,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: pu	Freq: 50
Form: pû	Freq: 1

207.
Lemma: premier	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: première	Freq: 242
Form: 1ère	Freq: 1

208.
Lemma: premier	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: premier	Freq: 226
Form: 1er	Freq: 5
Form: ier	Freq: 3
Form: 1e	Freq: 2

209.
Lemma: premier	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: premier	Freq: 17
Form: 1er	Freq: 1

210.
Lemma: procès	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: procès	Freq: 16
Form: procés	Freq: 1

211.
Lemma: professionnalisme	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: professionnalisme	Freq: 10
Form: professionalisme	Freq: 2
Form: pofessionnalisme	Freq: 1
Form: profesionnalisme	Freq: 1

212.
Lemma: professionnel	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: professionnelle	Freq: 18
Form: pro	Freq: 1

213.
Lemma: professionnel	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: professionnel	Freq: 30
Form: pro	Freq: 2
Form: professionel	Freq: 1

214.
Lemma: protocole	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: protocole	Freq: 6
Form: protocol	Freq: 1

215.
Lemma: prédécesseur	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: prédécesseur	Freq: 2
Form: prédecesseur	Freq: 1

216.
Lemma: préparer	MSD: pos=VERB,Gender=Masc,Number=Plur,Tense=Past,VerbForm=Part
Form: préparés	Freq: 3
Form: preparés	Freq: 1

217.
Lemma: présenter	MSD: pos=VERB,VerbForm=Inf
Form: présenter	Freq: 18
Form: presenter	Freq: 1

218.
Lemma: pâtisserie	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: patisserie	Freq: 3
Form: pâtisserie	Freq: 1

219.
Lemma: pèlerinage	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: pèlerinage	Freq: 6
Form: pélerinage	Freq: 1

220.
Lemma: périr	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: péri	Freq: 2
Form: pery	Freq: 1

221.
Lemma: quatorzième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: 14ème	Freq: 1
Form: xiv	Freq: 1

222.
Lemma: quatrième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: quatrième	Freq: 12
Form: 4	Freq: 1

223.
Lemma: quatrième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: quatrième	Freq: 12
Form: ivème	Freq: 1

224.
Lemma: que	MSD: pos=ADV,Polarity=Neg
Form: qu'	Freq: 4
Form: que	Freq: 1

225.
Lemma: que	MSD: pos=PRON,Gender=Masc,Number=Sing,PronType=Rel
Form: que	Freq: 8
Form: qu'	Freq: 1

226.
Lemma: que	MSD: pos=PRON,PronType=Rel
Form: qu'	Freq: 209
Form: que	Freq: 174

227.
Lemma: quel	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: quelle	Freq: 5
Form: quel	Freq: 1

228.
Lemma: quinzième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: 15éme	Freq: 1
Form: quinzième	Freq: 1

229.
Lemma: rebaptiser	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: rebaptisé	Freq: 3
Form: repabtisé	Freq: 1

230.
Lemma: recevoir	MSD: pos=VERB,Gender=Masc,Number=Sing,Tense=Past,VerbForm=Part
Form: reçu	Freq: 36
Form: recu	Freq: 1

231.
Lemma: recommander	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=1,Tense=Pres,VerbForm=Fin
Form: recommandons	Freq: 1
Form: recommendons	Freq: 1

232.
Lemma: reconnaître	MSD: pos=VERB,VerbForm=Inf
Form: reconnaître	Freq: 9
Form: reconnaitre	Freq: 2

233.
Lemma: reconstruction	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: reconstruction	Freq: 11
Form: recontruction	Freq: 1

234.
Lemma: redouter	MSD: pos=VERB,Tense=Pres,VerbForm=Part
Form: redout	Freq: 1
Form: redoutant	Freq: 1

235.
Lemma: registre	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: registre	Freq: 6
Form: régistre	Freq: 1

236.
Lemma: renouveler	MSD: pos=VERB,Gender=Fem,Number=Sing,Tense=Past,VerbForm=Part
Form: renouvelée	Freq: 3
Form: renouvellée	Freq: 1

237.
Lemma: renouveler	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: renouvelle	Freq: 1
Form: renouvèle	Freq: 1

238.
Lemma: rester	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: reste	Freq: 2
Form: restent	Freq: 2

239.
Lemma: retraite	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: retraite	Freq: 23
Form: retraire	Freq: 1

240.
Lemma: royal	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: royale	Freq: 19
Form: royal	Freq: 1

241.
Lemma: réception	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: réception	Freq: 7
Form: reception	Freq: 1

242.
Lemma: réduire	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: réduit	Freq: 2
Form: reduit	Freq: 1

243.
Lemma: réfutation	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: refutation	Freq: 1
Form: réfutation	Freq: 1

244.
Lemma: régional	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: régional	Freq: 13
Form: évident	Freq: 1

245.
Lemma: régner	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Past,VerbForm=Fin
Form: règna	Freq: 1
Form: régna	Freq: 1

246.
Lemma: révéler	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: révèlent	Freq: 1
Form: révélèrent	Freq: 1

247.
Lemma: rééducation	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: réeducation	Freq: 1
Form: rééducation	Freq: 1

248.
Lemma: saint	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: saints	Freq: 3
Form: ss.	Freq: 1

249.
Lemma: saint	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: saint	Freq: 18
Form: st	Freq: 1

250.
Lemma: saison	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: saison	Freq: 135
Form: saisoon	Freq: 1

251.
Lemma: samouraï	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: samourai	Freq: 1
Form: samouraï	Freq: 1

252.
Lemma: sceau	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: sceau	Freq: 2
Form: sceaux	Freq: 1

253.
Lemma: se	MSD: pos=PRON,Person=3,PronType=Prs
Form: se	Freq: 1190
Form: s'	Freq: 873

254.
Lemma: second	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: seconde	Freq: 63
Form: second	Freq: 1

255.
Lemma: sembler	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: semble	Freq: 19
Form: semblent	Freq: 9

256.
Lemma: septième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: septième	Freq: 4
Form: 7e	Freq: 2

257.
Lemma: septième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: 7e	Freq: 3
Form: septième	Freq: 3
Form: 7	Freq: 1

258.
Lemma: shérif	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: shérif	Freq: 2
Form: shériff	Freq: 1

259.
Lemma: signifier	MSD: pos=VERB,Tense=Pres,VerbForm=Part
Form: signifiant	Freq: 7
Form: sigifiant	Freq: 1

260.
Lemma: sixième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: sixième	Freq: 3
Form: 6e	Freq: 1
Form: 6ème	Freq: 1
Form: vi	Freq: 1

261.
Lemma: sixième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: sixième	Freq: 8
Form: 6e	Freq: 2

262.
Lemma: soeur	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: soeurs	Freq: 1
Form: sœurs	Freq: 1

263.
Lemma: soi	MSD: pos=PRON,Number=Sing,Person=1
Form: moi	Freq: 35
Form: -moi	Freq: 2

264.
Lemma: soi	MSD: pos=PRON,Number=Sing,Person=2
Form: toi	Freq: 4
Form: -toi	Freq: 2

265.
Lemma: son	MSD: pos=DET,Gender=Fem,Number=Plur,PronType=Prs
Form: ses	Freq: 300
Form: leurs	Freq: 80
Form: mes	Freq: 18
Form: nos	Freq: 17
Form: vos	Freq: 10

266.
Lemma: son	MSD: pos=DET,Gender=Fem,Number=Sing,PronType=Prs
Form: sa	Freq: 838
Form: son	Freq: 275
Form: leur	Freq: 209
Form: ma	Freq: 53
Form: notre	Freq: 42
Form: votre	Freq: 20
Form: mon	Freq: 8

267.
Lemma: son	MSD: pos=DET,Gender=Masc,Number=Plur,PronType=Prs
Form: ses	Freq: 412
Form: leurs	Freq: 110
Form: nos	Freq: 37
Form: mes	Freq: 16
Form: vos	Freq: 15
Form: tes	Freq: 1

268.
Lemma: son	MSD: pos=DET,Gender=Masc,Number=Sing,PronType=Prs
Form: son	Freq: 1074
Form: leur	Freq: 177
Form: mon	Freq: 53
Form: notre	Freq: 35
Form: votre	Freq: 28
Form: ton	Freq: 7
Form: sont	Freq: 1

269.
Lemma: son	MSD: pos=DET,Number=Plur,PronType=Prs
Form: ses	Freq: 14
Form: leurs	Freq: 1

270.
Lemma: son	MSD: pos=DET,Number=Sing,PronType=Prs
Form: son	Freq: 27
Form: leur	Freq: 8
Form: mon	Freq: 2
Form: notre	Freq: 1
Form: votre	Freq: 1

271.
Lemma: souhaiter	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: souhaite	Freq: 6
Form: souhaitent	Freq: 2

272.
Lemma: spermatozoïde	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: spermatozoides	Freq: 1
Form: spermatozoïdes	Freq: 1

273.
Lemma: sublime	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: sublime	Freq: 1
Form: sublîme	Freq: 1

274.
Lemma: succéder	MSD: pos=VERB,Tense=Pres,VerbForm=Part
Form: succèdant	Freq: 2
Form: succédant	Freq: 1

275.
Lemma: super	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: super	Freq: 4
Form: supers	Freq: 1

276.
Lemma: supporter	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: supporters	Freq: 3
Form: supporteurs	Freq: 1

277.
Lemma: sur	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: sur	Freq: 1
Form: sure	Freq: 1

278.
Lemma: suédois	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: suédois	Freq: 5
Form: suedois	Freq: 1

279.
Lemma: sympa	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: sympas	Freq: 5
Form: sympa	Freq: 1

280.
Lemma: sympathique	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: sympathique	Freq: 9
Form: sympatique	Freq: 1
Form: sympatiques	Freq: 1

281.
Lemma: sélection	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: sélection	Freq: 25
Form: selektion	Freq: 1

282.
Lemma: sœur	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: sœur	Freq: 19
Form: belle-sœur	Freq: 1

283.
Lemma: technologie	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: technologie	Freq: 18
Form: tech	Freq: 1

284.
Lemma: théorie	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: théorie	Freq: 28
Form: théori	Freq: 1

285.
Lemma: tome	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: tome	Freq: 3
Form: t.	Freq: 1

286.
Lemma: tout	MSD: pos=DET,Gender=Fem,Number=Sing
Form: toute	Freq: 110
Form: tte	Freq: 1

287.
Lemma: tranquillité	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: tranquilité	Freq: 1
Form: tranquillité	Freq: 1

288.
Lemma: tribunal	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: tribunaux	Freq: 3
Form: tribunals	Freq: 1

289.
Lemma: troisième	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: troisième	Freq: 33
Form: 3e	Freq: 1
Form: 3ème	Freq: 1

290.
Lemma: troisième	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: troisième	Freq: 31
Form: 3e	Freq: 2
Form: 3ème	Freq: 1

291.
Lemma: trouver	MSD: pos=VERB,Mood=Cnd,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: trouverait	Freq: 1
Form: trouveroit	Freq: 1

292.
Lemma: tu	MSD: pos=PRON,Number=Sing,Person=2,PronType=Prs
Form: -tu	Freq: 4
Form: tu	Freq: 1

293.
Lemma: tumulus	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: tumuli	Freq: 1
Form: tumulus	Freq: 1

294.
Lemma: télévision	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: télévision	Freq: 38
Form: telévision	Freq: 1

295.
Lemma: un	MSD: pos=DET,Definite=Ind,Gender=Fem,Number=Plur,PronType=Art
Form: des	Freq: 697
Form: de	Freq: 136
Form: d'	Freq: 37

296.
Lemma: un	MSD: pos=DET,Definite=Ind,Gender=Fem,Number=Sing,PronType=Art
Form: une	Freq: 3190
Form: de	Freq: 56
Form: d'	Freq: 5

297.
Lemma: un	MSD: pos=DET,Definite=Ind,Gender=Masc,Number=Plur,PronType=Art
Form: des	Freq: 857
Form: de	Freq: 150
Form: d'	Freq: 71

298.
Lemma: un	MSD: pos=DET,Definite=Ind,Gender=Masc,Number=Sing,PronType=Art
Form: un	Freq: 3678
Form: de	Freq: 48
Form: d'	Freq: 2
Form: in	Freq: 1

299.
Lemma: un	MSD: pos=DET,Definite=Ind,PronType=Art
Form: d'	Freq: 24
Form: de	Freq: 22

300.
Lemma: venir	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: vient	Freq: 4
Form: viennent	Freq: 3

301.
Lemma: venir	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: vient	Freq: 47
Form: viennent	Freq: 1

302.
Lemma: veto	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: veto	Freq: 4
Form: véto	Freq: 1

303.
Lemma: vidéo	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: vidéo	Freq: 3
Form: vidéos	Freq: 1

304.
Lemma: vieux	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: vieux	Freq: 16
Form: vieil	Freq: 7

305.
Lemma: voir	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: voit	Freq: 5
Form: voient	Freq: 2

306.
Lemma: vous	MSD: pos=PRON,Number=Plur,Person=2,PronType=Prs
Form: vous	Freq: 147
Form: -vous	Freq: 13

307.
Lemma: voûte	MSD: pos=NOUN,Gender=Fem,Number=Plur
Form: voutes	Freq: 1
Form: voûtes	Freq: 1

308.
Lemma: voûte	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: voute	Freq: 1
Form: voûte	Freq: 1

309.
Lemma: âgé	MSD: pos=ADJ,Gender=Masc,Number=Sing
Form: âgé	Freq: 8
Form: agé	Freq: 1

310.
Lemma: ça	MSD: pos=PRON,Gender=Masc,Number=Sing,Person=3,PronType=Dem
Form: ça	Freq: 31
Form: ca	Freq: 2

311.
Lemma: économie	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: économie	Freq: 38
Form: economie	Freq: 3

312.
Lemma: éducatif	MSD: pos=ADJ,Gender=Fem,Number=Sing
Form: educative	Freq: 1
Form: éducative	Freq: 1

313.
Lemma: égalité	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: égalité	Freq: 8
Form: egalité	Freq: 1

314.
Lemma: église	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: église	Freq: 149
Form: eglise	Freq: 3

315.
Lemma: électrique	MSD: pos=ADJ,Gender=Masc,Number=Plur
Form: électriques	Freq: 4
Form: éléctriques	Freq: 1

316.
Lemma: élever	MSD: pos=VERB,Gender=Fem,Number=Sing,Tense=Past,VerbForm=Part
Form: élevée	Freq: 4
Form: elevée	Freq: 1

317.
Lemma: émirat	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: emirats	Freq: 1
Form: émirats	Freq: 1

318.
Lemma: émission	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: émission	Freq: 25
Form: émmision	Freq: 1

319.
Lemma: état	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: états	Freq: 19
Form: etats	Freq: 6

320.
Lemma: état	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: état	Freq: 165
Form: etat	Freq: 12

321.
Lemma: étranger	MSD: pos=ADJ,Gender=Fem,Number=Plur
Form: étrangères	Freq: 17
Form: etrangères	Freq: 2

322.
Lemma: étudiant	MSD: pos=NOUN,Gender=Masc,Number=Plur
Form: étudiants	Freq: 19
Form: etudiants	Freq: 1

323.
Lemma: étudier	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: étudie	Freq: 14
Form: étude	Freq: 1

324.
Lemma: éviter	MSD: pos=VERB,VerbForm=Inf
Form: éviter	Freq: 31
Form: eviter	Freq: 1

325.
Lemma: évènement	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: évènement	Freq: 6
Form: évenement	Freq: 1

326.
Lemma: être	MSD: pos=AUX,Mood=Ind,Number=Plur,Person=3,Tense=Pres,VerbForm=Fin
Form: sont	Freq: 1019
Form: son	Freq: 3

327.
Lemma: être	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: est	Freq: 4509
Form: esr	Freq: 1

328.
Lemma: être	MSD: pos=AUX,Tense=Pres,VerbForm=Part
Form: étant	Freq: 86
Form: etant	Freq: 2

329.
Lemma: être	MSD: pos=AUX,VerbForm=Inf
Form: être	Freq: 340
Form: -être	Freq: 1
Form: etre	Freq: 1

330.
Lemma: île	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: île	Freq: 77
Form: presqu'île	Freq: 4

331.
Lemma: îlot	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: îlot	Freq: 3
Form: ilot	Freq: 1

332.
Lemma: œuf	MSD: pos=NOUN,Gender=Masc,Number=Sing
Form: œuf	Freq: 3
Form: oeuf	Freq: 1

333.
Lemma: œuvre	MSD: pos=NOUN,Gender=Fem,Number=Sing
Form: œuvre	Freq: 69
Form: main-d'œuvre	Freq: 5
Form: chef-d'œuvre	Freq: 1
Form: manœuvre	Freq: 1
Form: oeuvre	Freq: 1

334.
Lemma: œuvrer	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: œuvre	Freq: 3
Form: oeuvre	Freq: 1
