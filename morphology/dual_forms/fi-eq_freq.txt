1.
Lemma: albumi	MSD: pos=NOUN,Case=Ade,Number=Sing
Form: -albumilla	Freq: 1
Form: albumilla	Freq: 1

2.
Lemma: ammattikorkeakoulutasoinen	MSD: pos=ADJ,Case=Nom,Degree=Pos,Number=Sing
Form: ammattikorkeakoulu-tasoinen	Freq: 1
Form: ammattikorkeakoulutasoinen	Freq: 1

3.
Lemma: analyysi	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: analyysi	Freq: 1
Form: analyysi-	Freq: 1

4.
Lemma: analyytikko	MSD: pos=NOUN,Case=Gen,Number=Plur
Form: analyytikkojen	Freq: 1
Form: analyytikoiden	Freq: 1

5.
Lemma: asiakas	MSD: pos=NOUN,Case=Gen,Number=Plur,Person[psor]=3
Form: asiakkaidensa	Freq: 1
Form: asiakkaittensa	Freq: 1

6.
Lemma: askel	MSD: pos=NOUN,Case=Par,Number=Plur
Form: askeleita	Freq: 1
Form: askelia	Freq: 1

7.
Lemma: azorit	MSD: pos=PROPN,Case=Gen,Number=Plur
Form: azoreiden	Freq: 1
Form: azorien	Freq: 1

8.
Lemma: cosplay	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: cosplay	Freq: 1
Form: cosplay-	Freq: 1

9.
Lemma: ei#minä	MSD: pos=VERB,Number=Sing,Person=1,Polarity=Neg,Style=Coll,VerbForm=Fin,Voice=Act
Form: emmä	Freq: 1
Form: emmää	Freq: 1

10.
Lemma: erilainen	MSD: pos=ADJ,Case=Par,Degree=Pos,Number=Plur,Typo=Yes
Form: erilaista	Freq: 1
Form: erillaisia	Freq: 1

11.
Lemma: finlandia#talo	MSD: pos=PROPN,Case=Ine,Number=Sing
Form: finlandia-talossa	Freq: 1
Form: finlandia-talossa.	Freq: 1

12.
Lemma: haku	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: haku	Freq: 1
Form: haku-	Freq: 1

13.
Lemma: halata	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin,Voice=Act
Form: halaa	Freq: 1
Form: halajaa	Freq: 1

14.
Lemma: hiili	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: hiili	Freq: 1
Form: hiili-	Freq: 1

15.
Lemma: hyvä	MSD: pos=ADJ,Case=Nom,Degree=Sup,Number=Plur
Form: parhaat	Freq: 2
Form: parhaimmat	Freq: 2

16.
Lemma: hyökkäys	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: hyökkäys	Freq: 1
Form: hyökkäys-	Freq: 1

17.
Lemma: hän	MSD: pos=PRON,Case=Nom,Number=Sing,Person=3,PronType=Prs,Style=Coll
Form: häm	Freq: 2
Form: hää	Freq: 2

18.
Lemma: ihan	MSD: pos=ADV,Style=Coll
Form: iha	Freq: 1
Form: ihav	Freq: 1

19.
Lemma: il	MSD: pos=PROPN,Case=Gen,Number=Sing
Form: il:n	Freq: 1
Form: ilin	Freq: 1

20.
Lemma: jatko	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: jatko	Freq: 1
Form: jatko-	Freq: 1

21.
Lemma: järjestö	MSD: pos=NOUN,Case=Gen,Number=Sing
Form: -järjestön	Freq: 1
Form: järjestön	Freq: 1

22.
Lemma: kamera	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: kamera	Freq: 1
Form: kamera-	Freq: 1

23.
Lemma: kokoinen	MSD: pos=ADJ,Case=Nom,Degree=Pos,Number=Sing
Form: -kokoinen	Freq: 1
Form: kokoinen	Freq: 1

24.
Lemma: kokoomus	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: kok.	Freq: 2
Form: kokoomus	Freq: 2

25.
Lemma: komponentti	MSD: pos=NOUN,Case=Par,Number=Plur
Form: komponentteja	Freq: 1
Form: komponetteja	Freq: 1

26.
Lemma: konsepti	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: -konsepti	Freq: 1
Form: konsepti	Freq: 1

27.
Lemma: kukaan	MSD: pos=PRON,Case=Ade,Number=Sing,PronType=Ind
Form: kellään	Freq: 2
Form: kenelläkään	Freq: 2

28.
Lemma: kukaan	MSD: pos=PRON,Case=Par,Number=Sing,PronType=Ind,Style=Coll
Form: kettää	Freq: 1
Form: ketää	Freq: 1

29.
Lemma: kuljetus	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: kuljetus	Freq: 1
Form: kuljetus-	Freq: 1

30.
Lemma: kumppanuus	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: kumppanuus	Freq: 1
Form: kumppanuus-	Freq: 1

31.
Lemma: kups	MSD: pos=PROPN,Case=Gen,Number=Sing
Form: kups:in	Freq: 1
Form: kupsin	Freq: 1

32.
Lemma: kuulua	MSD: pos=VERB,Mood=Ind,Number=Plur,Person=3,Tense=Pres,Typo=Yes,VerbForm=Fin,Voice=Act
Form: kuulu	Freq: 1
Form: ovat	Freq: 1

33.
Lemma: kyyti	MSD: pos=NOUN,Case=Ill,Number=Sing
Form: kyytiin	Freq: 1
Form: kyytssss	Freq: 1

34.
Lemma: lehti	MSD: pos=NOUN,Case=Ela,Number=Sing
Form: -lehdestä	Freq: 1
Form: lehdestä	Freq: 1

35.
Lemma: lehti	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: -lehti	Freq: 3
Form: lehti	Freq: 3

36.
Lemma: levy#yhtiö	MSD: pos=NOUN,Case=All,Number=Sing
Form: -levy-yhtiölle	Freq: 1
Form: levy-yhtiölle	Freq: 1

37.
Lemma: lisenssi	MSD: pos=NOUN,Case=Ade,Number=Sing
Form: -lisenssillä	Freq: 1
Form: lisenssillä	Freq: 1

38.
Lemma: lääkäri	MSD: pos=NOUN,Case=Gen,Number=Plur
Form: lääkäreiden	Freq: 1
Form: lääkärien	Freq: 1

39.
Lemma: media	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: media	Freq: 1
Form: media-	Freq: 1

40.
Lemma: meikki	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: meikki	Freq: 1
Form: meikki-	Freq: 1

41.
Lemma: mennä	MSD: pos=VERB,Mood=Cnd,Number=Sing,Person=0,VerbForm=Fin,Voice=Act
Form: menis	Freq: 1
Form: menisi	Freq: 1

42.
Lemma: minä	MSD: pos=PRON,Case=Gen,Number=Plur,Person=1,PronType=Prs,Style=Coll
Form: meiäm	Freq: 1
Form: meiän	Freq: 1

43.
Lemma: mukavuus	MSD: pos=NOUN,Case=Par,Number=Sing
Form: -mukavuutta	Freq: 1
Form: mukavuutta	Freq: 1

44.
Lemma: musiikki	MSD: pos=NOUN,Case=Ill,Number=Sing
Form: -musiikkiin	Freq: 1
Form: musiikkiin	Freq: 1

45.
Lemma: mutainen	MSD: pos=ADJ,Case=Gen,Degree=Pos,Number=Plur
Form: mutaisien	Freq: 1
Form: mutaisten	Freq: 1

46.
Lemma: mänty	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: mänty	Freq: 1
Form: mänty-	Freq: 1

47.
Lemma: nerehta	MSD: pos=PROPN,Case=Nom,Number=Sing
Form: nerehta	Freq: 1
Form: nerehta-	Freq: 1

48.
Lemma: niin	MSD: pos=SCONJ,Style=Coll
Form: ni	Freq: 1
Form: nii	Freq: 1

49.
Lemma: nominatiivi	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: nominatiivi	Freq: 1
Form: nominativi	Freq: 1

50.
Lemma: nähdä	MSD: pos=VERB,InfForm=1,Number=Sing,Style=Coll,VerbForm=Inf,Voice=Act
Form: nähä	Freq: 1
Form: näkeä	Freq: 1

51.
Lemma: olla	MSD: pos=AUX,Mood=Pot,Number=Plur,Person=3,VerbForm=Fin,Voice=Act
Form: lie	Freq: 2
Form: lienevät	Freq: 2

52.
Lemma: openoffice	MSD: pos=PROPN,Case=Gen,Number=Sing,Typo=Yes
Form: cen	Freq: 1
Form: openoffi	Freq: 1

53.
Lemma: opetus	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: opetus	Freq: 2
Form: opetus-	Freq: 2

54.
Lemma: patentti	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: patentti	Freq: 1
Form: patentti-	Freq: 1

55.
Lemma: pitkä	MSD: pos=ADJ,Case=Nom,Degree=Cmp,Number=Sing
Form: pidempi	Freq: 1
Form: pitempi	Freq: 1

56.
Lemma: populaatio	MSD: pos=NOUN,Case=Gen,Number=Sing
Form: -populaation	Freq: 1
Form: populaation	Freq: 1

57.
Lemma: porkkana	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: porkkana	Freq: 1
Form: porkkana-	Freq: 1

58.
Lemma: päälle	MSD: pos=ADV,Person[psor]=3
Form: päälleen	Freq: 1
Form: päällensä	Freq: 1

59.
Lemma: päällä	MSD: pos=ADV,Person[psor]=3
Form: päällänsä	Freq: 1
Form: päällään	Freq: 1

60.
Lemma: rauta	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: rauta	Freq: 1
Form: rauta-	Freq: 1

61.
Lemma: ruoka	MSD: pos=NOUN,Case=Tra,Number=Sing
Form: ruoaksi	Freq: 1
Form: ruuaksi	Freq: 1

62.
Lemma: siinä	MSD: pos=ADV,Style=Coll
Form: siim	Freq: 1
Form: siin	Freq: 1

63.
Lemma: silleen	MSD: pos=ADV,Style=Coll
Form: sillee	Freq: 1
Form: silleen	Freq: 1

64.
Lemma: sopimus	MSD: pos=NOUN,Case=Ade,Number=Plur
Form: -sopimuksilla	Freq: 1
Form: sopimuksilla	Freq: 1

65.
Lemma: symposiumi	MSD: pos=NOUN,Case=Gen,Number=Sing
Form: -symposiumin	Freq: 1
Form: symposiumin	Freq: 1

66.
Lemma: säveltäjä	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: -säveltäjä	Freq: 1
Form: säveltäjä	Freq: 1

67.
Lemma: taakse	MSD: pos=ADP,AdpType=Post,Person[psor]=3
Form: taakseen	Freq: 1
Form: taaksensa	Freq: 1

68.
Lemma: tie	MSD: pos=NOUN,Case=Ade,Number=Sing
Form: -tiellä	Freq: 1
Form: tiellä	Freq: 1

69.
Lemma: tuntea	MSD: pos=VERB,Case=Gen,Degree=Pos,Number=Sing,PartForm=Pres,Typo=Yes,VerbForm=Part,Voice=Act
Form: tuntevansa	Freq: 1
Form: tuntevat	Freq: 1

70.
Lemma: tyy	MSD: pos=PROPN,Case=Gen,Number=Sing
Form: tyy:n	Freq: 1
Form: tyyn	Freq: 1

71.
Lemma: tyyppinen	MSD: pos=ADJ,Case=Nom,Degree=Pos,Number=Sing
Form: -tyyppinen	Freq: 1
Form: tyyppinen	Freq: 1

72.
Lemma: tämä	MSD: pos=PRON,Case=Ill,Clitic=Kin,Number=Sing,PronType=Dem
Form: tähän(kin)	Freq: 1
Form: tähänkin	Freq: 1

73.
Lemma: vaalitulos	MSD: pos=NOUN,Case=Gen,Number=Plur
Form: vaalituloksien	Freq: 1
Form: vaalitulosten	Freq: 1

74.
Lemma: valuutta	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: valuutta	Freq: 1
Form: valuutta-	Freq: 1

75.
Lemma: viettää	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin,Voice=Pass
Form: vietetettiin	Freq: 1
Form: vietettiin	Freq: 1

76.
Lemma: vihreä	MSD: pos=NOUN,Case=Nom,Number=Plur
Form: vihr	Freq: 1
Form: vihreät	Freq: 1

77.
Lemma: voida	MSD: pos=AUX,Mood=Cnd,Number=Plur,Person=1,Style=Coll,VerbForm=Fin,Voice=Act
Form: voitaisiin	Freq: 1
Form: voitas	Freq: 1

78.
Lemma: wto	MSD: pos=NOUN,Abbr=Yes,Case=Ill,Number=Sing
Form: wto:	Freq: 1
Form: wto:iin	Freq: 1

79.
Lemma: yhtiö	MSD: pos=NOUN,Case=Gen,Number=Sing,Typo=Yes
Form: htiön	Freq: 1
Form: y	Freq: 1

80.
Lemma: yhtye	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: -yhtye	Freq: 9
Form: yhtye	Freq: 9

81.
Lemma: yhtään	MSD: pos=ADV,Style=Coll
Form: yhttään	Freq: 1
Form: yhtää	Freq: 1

82.
Lemma: yht’äkkiä	MSD: pos=ADV,Typo=Yes
Form: yht’	Freq: 1
Form: äkkiä	Freq: 1

83.
Lemma: yksikkö	MSD: pos=NOUN,Case=Ine,Number=Sing
Form: -yksikössä	Freq: 1
Form: yksikössä	Freq: 1

84.
Lemma: yksinmyynti	MSD: pos=NOUN,Case=Nom,Number=Sing
Form: yksinmyynti	Freq: 1
Form: yksinmyynti-	Freq: 1

85.
Lemma: äänestäjä	MSD: pos=NOUN,Case=Nom,Number=Plur
Form: -äänestäjät	Freq: 1
Form: äänestäjät	Freq: 1
