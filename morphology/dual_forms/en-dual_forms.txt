1.
Lemma: a	MSD: pos=DET,Definite=Ind,PronType=Art
Form: a	Freq: 3740
Form: an	Freq: 494

2.
Lemma: answer	MSD: pos=VERB,VerbForm=Inf
Form: answer	Freq: 10
Form: answers	Freq: 1

3.
Lemma: be	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: is	Freq: 1905
Form: 's	Freq: 300
Form: s	Freq: 68
Form: ’s	Freq: 9
Form: ai	Freq: 3

4.
Lemma: be	MSD: pos=AUX,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: are	Freq: 986
Form: 'm	Freq: 196
Form: 're	Freq: 101
Form: m	Freq: 15
Form: r	Freq: 14
Form: re	Freq: 5
Form: ai	Freq: 1
Form: is	Freq: 1
Form: ’m	Freq: 1

5.
Lemma: be	MSD: pos=AUX,VerbForm=Inf
Form: be	Freq: 1030
Form: am	Freq: 2
Form: are	Freq: 1

6.
Lemma: be	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: is	Freq: 331
Form: 's	Freq: 43
Form: s	Freq: 6
Form: ’s	Freq: 3

7.
Lemma: be	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: are	Freq: 136
Form: 'm	Freq: 1
Form: m	Freq: 1

8.
Lemma: bear	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: beared	Freq: 1
Form: bore	Freq: 1

9.
Lemma: beaver	MSD: pos=NOUN,Number=Plur
Form: beaver	Freq: 1
Form: beavers	Freq: 1

10.
Lemma: become	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: become	Freq: 22
Form: became	Freq: 1

11.
Lemma: break	MSD: pos=VERB,VerbForm=Inf
Form: break	Freq: 5
Form: broken	Freq: 1

12.
Lemma: can	MSD: pos=AUX,VerbForm=Fin
Form: can	Freq: 630
Form: ca	Freq: 78

13.
Lemma: charge	MSD: pos=VERB,Tense=Past,VerbForm=Part,Voice=Pass
Form: charged	Freq: 7
Form: charge	Freq: 1

14.
Lemma: chicken	MSD: pos=NOUN,Number=Plur
Form: chickens	Freq: 10
Form: chicken	Freq: 2

15.
Lemma: choose	MSD: pos=VERB,VerbForm=Inf
Form: choose	Freq: 10
Form: chose	Freq: 1

16.
Lemma: come	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: comes	Freq: 34
Form: come	Freq: 1

17.
Lemma: company	MSD: pos=NOUN,Number=Plur
Form: companies	Freq: 17
Form: company's	Freq: 1

18.
Lemma: deal	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: deal	Freq: 1
Form: dealt	Freq: 1

19.
Lemma: do	MSD: pos=AUX,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: do	Freq: 477
Form: du	Freq: 1

20.
Lemma: drive	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: drove	Freq: 2
Form: drive	Freq: 1

21.
Lemma: eat	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: eaten	Freq: 7
Form: ate	Freq: 1

22.
Lemma: extracurricular	MSD: pos=ADJ,Degree=Pos
Form: extracurricular	Freq: 2
Form: extrcurricular	Freq: 1

23.
Lemma: feed	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: fed	Freq: 1
Form: feed	Freq: 1

24.
Lemma: feel	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: felt	Freq: 25
Form: feel	Freq: 1

25.
Lemma: fish	MSD: pos=NOUN,Number=Plur
Form: fish	Freq: 6
Form: fishes	Freq: 1

26.
Lemma: fly	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: fly	Freq: 2
Form: flew	Freq: 1

27.
Lemma: follow	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: follows	Freq: 12
Form: follow	Freq: 1

28.
Lemma: get	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: get	Freq: 57
Form: got	Freq: 1

29.
Lemma: get	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: got	Freq: 12
Form: gotten	Freq: 7

30.
Lemma: give	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: given	Freq: 29
Form: give	Freq: 1

31.
Lemma: grocery	MSD: pos=NOUN,Number=Plur
Form: groceries	Freq: 1
Form: grocerys	Freq: 1

32.
Lemma: have	MSD: pos=AUX,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: has	Freq: 354
Form: 's	Freq: 1

33.
Lemma: have	MSD: pos=AUX,Mood=Ind,Tense=Past,VerbForm=Fin
Form: had	Freq: 153
Form: 'd	Freq: 8

34.
Lemma: have	MSD: pos=AUX,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: have	Freq: 474
Form: 've	Freq: 123
Form: ve	Freq: 7
Form: v	Freq: 1
Form: ’ve	Freq: 1

35.
Lemma: have	MSD: pos=AUX,VerbForm=Inf
Form: have	Freq: 111
Form: 've	Freq: 1

36.
Lemma: have	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: has	Freq: 165
Form: haves	Freq: 1

37.
Lemma: have	MSD: pos=VERB,VerbForm=Inf
Form: have	Freq: 319
Form: had	Freq: 1

38.
Lemma: head	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: headed	Freq: 2
Form: head	Freq: 1

39.
Lemma: hope	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: hope	Freq: 59
Form: hoped	Freq: 1

40.
Lemma: involve	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: involved	Freq: 5
Form: $involved	Freq: 1

41.
Lemma: know	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: know	Freq: 102
Form: knew	Freq: 1

42.
Lemma: know	MSD: pos=VERB,VerbForm=Inf
Form: know	Freq: 242
Form: no	Freq: 1

43.
Lemma: lay	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: laid	Freq: 1
Form: lay	Freq: 1

44.
Lemma: learn	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: learned	Freq: 5
Form: learnt	Freq: 1

45.
Lemma: live	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: lived	Freq: 8
Form: live	Freq: 1

46.
Lemma: lose	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: lost	Freq: 8
Form: loss	Freq: 1

47.
Lemma: mean	MSD: pos=VERB,VerbForm=Inf
Form: mean	Freq: 14
Form: meant	Freq: 1

48.
Lemma: min	MSD: pos=NOUN,Number=Plur
Form: mins	Freq: 6
Form: min	Freq: 2

49.
Lemma: mystery	MSD: pos=NOUN,Number=Plur
Form: mysteries	Freq: 1
Form: mysterys	Freq: 1

50.
Lemma: name	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: named	Freq: 14
Form: name	Freq: 1

51.
Lemma: old	MSD: pos=ADJ,Degree=Pos
Form: old	Freq: 76
Form: ã³l	Freq: 1

52.
Lemma: pay	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: paid	Freq: 13
Form: payed	Freq: 1

53.
Lemma: people	MSD: pos=NOUN,Number=Plur
Form: people	Freq: 238
Form: peoples	Freq: 9

54.
Lemma: place	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: placed	Freq: 2
Form: place	Freq: 1

55.
Lemma: prefer	MSD: pos=VERB,VerbForm=Inf
Form: prefer	Freq: 2
Form: prefere	Freq: 1

56.
Lemma: prove	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: proven	Freq: 3
Form: proved	Freq: 1

57.
Lemma: rest	MSD: pos=VERB,VerbForm=Inf
Form: rest	Freq: 1
Form: rests	Freq: 1

58.
Lemma: run	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: run	Freq: 8
Form: ran	Freq: 2

59.
Lemma: save	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: saved	Freq: 2
Form: save	Freq: 1

60.
Lemma: see	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: seen	Freq: 28
Form: see	Freq: 1

61.
Lemma: show	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: showed	Freq: 13
Form: show	Freq: 1

62.
Lemma: show	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: shown	Freq: 3
Form: showed	Freq: 1

63.
Lemma: sleep	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: sleep	Freq: 3
Form: slept	Freq: 1

64.
Lemma: staff	MSD: pos=NOUN,Number=Plur
Form: staff	Freq: 15
Form: staffs	Freq: 1

65.
Lemma: strike	MSD: pos=VERB,VerbForm=Inf
Form: strike	Freq: 3
Form: struck	Freq: 1

66.
Lemma: take	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: taken	Freq: 25
Form: took	Freq: 1

67.
Lemma: talk	MSD: pos=VERB,VerbForm=Inf
Form: talk	Freq: 38
Form: talked	Freq: 1

68.
Lemma: total	MSD: pos=VERB,VerbForm=Ger
Form: totaling	Freq: 1
Form: totalling	Freq: 1

69.
Lemma: travel	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: traveled	Freq: 4
Form: travelled	Freq: 2

70.
Lemma: travel	MSD: pos=VERB,Tense=Pres,VerbForm=Part
Form: traveling	Freq: 3
Form: travelling	Freq: 1

71.
Lemma: travel	MSD: pos=VERB,VerbForm=Ger
Form: travelling	Freq: 7
Form: traveling	Freq: 5

72.
Lemma: travel	MSD: pos=VERB,VerbForm=Inf
Form: travel	Freq: 5
Form: travels	Freq: 1

73.
Lemma: turn	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: turn	Freq: 1
Form: turned	Freq: 1

74.
Lemma: use	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: used	Freq: 36
Form: use	Freq: 1

75.
Lemma: use	MSD: pos=VERB,VerbForm=Inf
Form: use	Freq: 83
Form: used	Freq: 1

76.
Lemma: wake	MSD: pos=VERB,Tense=Past,VerbForm=Part
Form: waked	Freq: 1
Form: woken	Freq: 1

77.
Lemma: want	MSD: pos=VERB,Mood=Ind,Number=Sing,Person=3,Tense=Pres,VerbForm=Fin
Form: wants	Freq: 22
Form: want	Freq: 1

78.
Lemma: want	MSD: pos=VERB,Mood=Ind,Tense=Past,VerbForm=Fin
Form: wanted	Freq: 57
Form: wants	Freq: 1

79.
Lemma: want	MSD: pos=VERB,Mood=Ind,Tense=Pres,VerbForm=Fin
Form: want	Freq: 116
Form: what	Freq: 1

80.
Lemma: will	MSD: pos=AUX,VerbForm=Fin
Form: will	Freq: 834
Form: 'll	Freq: 149
Form: wo	Freq: 48
Form: ll	Freq: 2
Form: wilt	Freq: 2

81.
Lemma: would	MSD: pos=AUX,VerbForm=Fin
Form: would	Freq: 600
Form: 'd	Freq: 44
Form: d	Freq: 1
Form: ould	Freq: 1

82.
Lemma: youth	MSD: pos=NOUN,Number=Plur
Form: youth	Freq: 1
Form: youths	Freq: 1
